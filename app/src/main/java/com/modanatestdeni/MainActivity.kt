package com.modanatestdeni

import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.Window
import com.modanatestdeni.R
import com.ncorti.slidetoact.SlideToActView
import com.ncorti.slidetoact.SlideToActView.OnSlideCompleteListener
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), OnSlideCompleteListener {
    var counter = 0
    lateinit var prefs: SharedPreferences

    val COUNTER = "counter"
    val SHARED_PREFERENCE_KEY = "com.modanatestdeni.MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_main)

        prefs = getSharedPreferences(
            SHARED_PREFERENCE_KEY, MODE_PRIVATE)
        counter = prefs.getInt(COUNTER, 0)

        if (counter % 2 == 1) {
            sliding_counter_sta.isReversed = true
            sliding_counter_sta.outerColor = ContextCompat.getColor(this, R.color.orange);
            sliding_counter_sta.text = resources.getText(R.string.slide_to_stop)
        }

        sliding_counter_sta.onSlideCompleteListener = this
        sliding_counter_et.setText(counter.toString())
    }

    override fun onSlideComplete(view: SlideToActView) {
        counter++
        if (view.isReversed) {
            view.resetSlider()
            view.isReversed = false
            view.outerColor = ContextCompat.getColor(this, R.color.lightBlue);
            view.text = resources.getText(R.string.slide_to_start)
            sliding_counter_et.setText(counter.toString())
        } else {
            view.resetSlider()
            view.isReversed = true
            view.outerColor = ContextCompat.getColor(this, R.color.orange);
            view.text = resources.getText(R.string.slide_to_stop)
            sliding_counter_et.setText(counter.toString())
        }
    }

    override fun onStop() {
        super.onStop()
        prefs.edit()
            .putInt(
                COUNTER,
                counter
            )
            .commit()
    }
}
